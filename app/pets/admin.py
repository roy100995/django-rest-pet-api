from django.contrib import admin
from pets import models


admin.site.register(models.Type)
admin.site.register(models.Person)
admin.site.register(models.Pet)
admin.site.register(models.PetDetail)
admin.site.register(models.Adopt)
admin.site.register(models.DonationType)
admin.site.register(models.Bank)
admin.site.register(models.Donation)
