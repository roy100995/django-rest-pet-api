from pets import serializers, models
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from django.contrib.auth import get_user_model


class UserViewset(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = serializers.UserSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(id=self.request.user.id)


class TypeViewset(viewsets.ModelViewSet):
    queryset = models.Type.objects.all()
    serializer_class = serializers.TypeSerializer


class PersonViewset(viewsets.ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer


class PetViewset(viewsets.ModelViewSet):
    queryset = models.Pet.objects.all()
    serializer_class = serializers.PetSerializer

    def _params_to_ints(self, qs):
        return [int(str_id) for str_id in qs.split(',')]

    def get_queryset(self):
        type = self.request.query_params.get('type')
        queryset = self.queryset
        if type:
            type_ids = self._params_to_ints(type)
            queryset = queryset.filter(type__id__in=type_ids)

        return queryset


class PetDetailViewset(viewsets.ModelViewSet):
    queryset = models.PetDetail.objects.all()
    serializer_class = serializers.PetDetailSerializer


class AdoptViewset(viewsets.ModelViewSet):
    queryset = models.Adopt.objects.all()
    serializer_class = serializers.AdoptSerializer


class DonationTypeViewset(viewsets.ModelViewSet):
    queryset = models.DonationType.objects.all()
    serializer_class = serializers.DonationTypeSerializer


class BankViewset(viewsets.ModelViewSet):
    queryset = models.Bank.objects.all()
    serializer_class = serializers.BankSerializer


class DonationViewset(viewsets.ModelViewSet):
    queryset = models.Donation.objects.all()
    serializer_class = serializers.DonationSerializer
