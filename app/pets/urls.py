from django.urls import path, include
from pets import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('users', views.UserViewset)
router.register('types', views.TypeViewset)
router.register('persons', views.PersonViewset)
router.register('pets', views.PetViewset)
router.register('pet-details', views.PetDetailViewset)
router.register('adopts', views.AdoptViewset)
router.register('donation-types', views.DonationTypeViewset)
router.register('banks', views.BankViewset)
router.register('donations', views.DonationViewset)

urlpatterns = [
    path('', include(router.urls))
]
