from rest_framework import serializers
from pets import models
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username', 'first_name', 'last_name', 'email']


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = '__all__'


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Person
        fields = '__all__'


class PetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Pet
        fields = '__all__'


class PetDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PetDetail
        fields = '__all__'


class AdoptSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Adopt
        fields = ['id', 'register_date', 'pet', 'person']


class DonationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DonationType
        fields = '__all__'


class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Bank
        fields = '__all__'


class DonationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Donation
        fields = '__all__'
