from django.db import models
from django.contrib.auth.models import User


class Type(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=9)
    email = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name


class Pet(models.Model):
    type = models.ForeignKey(
        Type, on_delete=models.SET_NULL, null=True, blank=True
    )
    person = models.ForeignKey(
        Person, on_delete=models.SET_NULL, null=True, blank=True
    )
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    price = models.DecimalField(
        max_digits=14, decimal_places=4, null=True, default=0)
    history = models.CharField(max_length=500)
    photo = models.ImageField(
        upload_to='pets/', null=True
    )
    is_adopted = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class PetDetail(models.Model):
    pet = models.ForeignKey(
        Pet, on_delete=models.CASCADE
    )
    key = models.CharField(max_length=50)
    value = models.CharField(max_length=100)

    def __str__(self):
        return self.key


class Adopt(models.Model):
    pet = models.ForeignKey(
        Pet, on_delete=models.SET_NULL, null=True, blank=True
    )
    person = models.ForeignKey(
        Person, on_delete=models.SET_NULL, null=True, blank=True
    )
    register_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.register_date)


class DonationType(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Bank(models.Model):
    name = models.CharField(max_length=100)
    person = models.CharField(max_length=100)
    number = models.CharField(max_length=15)
    dni = models.CharField(max_length=15)
    is_active = models.BooleanField()

    def __str__(self):
        return self.name


class Donation(models.Model):
    donation_type = models.ForeignKey(
        DonationType, on_delete=models.SET_NULL, null=True, blank=True
    )
    person = models.ForeignKey(
        Person, on_delete=models.SET_NULL, null=True, blank=True
    )
    bank = models.ForeignKey(
        Bank, on_delete=models.SET_NULL, null=True, blank=True
    )
    image = models.ImageField(upload_to='donations/', null=True)
    message = models.CharField(max_length=255)
    donation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.donation_date)
